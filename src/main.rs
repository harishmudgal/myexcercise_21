use std::fs::{self, File};
use std::io::BufReader;
use std::io::Write;
use std::io::{self, BufRead};
use std::collections::HashMap;

extern crate clap;

use clap::{Arg, App};
struct Person {
  Fin: String,
  Am: String,

}

fn main() {

  let matches = App::new("My Training Program")
  .version("0.1.0")
  .author("XYZ")
  .about("Teaches me argument parsing")
  .arg(Arg::with_name("file")
           .short("f")
           .long("file")
           .takes_value(true)
           .help("A cool file"))
  .arg(Arg::with_name("out")
           .short("o")
           .long("out")
           .takes_value(true)
           .help("Five less than your favorite number"))
  .get_matches();

  let myfile = matches.value_of("file").unwrap_or("input.txt");
  let ofile = matches.value_of("out").unwrap_or("output.txt");
    let f = File::open(myfile);

    let f3 = BufReader::new(f.expect("file name is incorrect"));

    let mut f1 = File::create(ofile).expect("unable to create");
    // let y: i32 = Y.parse().unwrap();
    let mut cnt = 0;
    let mut amt = HashMap::new();

 for line in f3.lines() {
        //println!("{:?}",line);
        //     println!("in main loop");
            let s = line.expect("N");
            println!("{:?}",s);
            let tokens: Vec<&str> = s.split(",").collect();
            let person = Person{Fin:String::from(tokens[0]), Am:String::from(tokens[2])};
          //   let name = tokens[0].to_string();
          // println!("{:?}", tokens[2]);
             let mut tk: i32 = person.Am.parse().unwrap();
              if cnt != 0 {
                      if amt.contains_key(&person.Fin) {
                           let val= amt.get(&person.Fin).expect("error");
                           tk = *val + tk;
                          //  match amt.get(&name) {
                          //         Some(v) => {
                          //                  tk = *v + tk;
                                          // println!("{}",tk);
                          //                   }
                          //         None => println!("error")
                          //       }

                              // println!("{}",tk);

                        }
                   }

              //println!("{}",tk);

              amt.insert(person.Fin,tk);

               cnt = cnt + 1;

   }
 for (key,value) in amt.iter() {
writeln!(f1, "{},{}", key, value).expect("cannot write");
 // println!("{}",value);
 }
//println!("{:?}",amt);
}
